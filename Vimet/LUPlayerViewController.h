//
//  LUPlayerViewController.h
//  Vimet
//
//  Created by Shuya Honda on 2014/07/04.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "LMMediaPlayerView.h"

@interface LUPlayerViewController : UIViewController<LMMediaPlayerViewDelegate>

@end
