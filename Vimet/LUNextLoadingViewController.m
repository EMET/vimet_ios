//
//  LUNextLoadingViewController.m
//  Vimet
//
//  Created by Shuya Honda on 2014/07/06.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import "LUNextLoadingViewController.h"
#import "FLAnimatedImage.h"
#import "FLAnimatedImageView.h"

@interface LUNextLoadingViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImage;
@property (weak, nonatomic) IBOutlet UILabel     *videoTitle;
@property (weak, nonatomic) IBOutlet FLAnimatedImageView *loadingGif;

@end

@implementation LUNextLoadingViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    /*
    NSURL *gifUrl = [[NSBundle mainBundle] URLForResource:@"" withExtension:@"gif"];
    FLAnimatedImage *gifImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:[NSData dataWithContentsOfURL:gifUrl]];
    FLAnimatedImageView *animationView = [[FLAnimatedImageView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    animationView.animatedImage = gifImage;
    [self.view addSubview:animationView];
     */
//    [self startLoadingImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setImage:(UIImage *)image{
    self.thumbnailImage.image = image;
}

- (void)setTitle:(NSString *)title{
    self.videoTitle.text = title;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)startLoadingImage
{
    self.loadingGif.opaque = YES;
    NSURL *gifUrl = [[NSBundle mainBundle] URLForResource:@"" withExtension:@"gif"];
    FLAnimatedImage *gifImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:[NSData dataWithContentsOfURL:gifUrl]];
    self.loadingGif.animatedImage = gifImage;
}

- (void)stopLoadingImage
{
    self.loadingGif.opaque = NO;
    [self.loadingGif stopAnimating];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
