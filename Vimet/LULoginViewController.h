//
//  LULoginViewController.h
//  Vimet
//
//  Created by Shuya Honda on 2014/06/26.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface LULoginViewController : UIViewController<FBLoginViewDelegate>

@end
