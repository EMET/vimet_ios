//
//  LUNextLoadingViewController.h
//  Vimet
//
//  Created by Shuya Honda on 2014/07/06.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LUNextLoadingViewController : UIViewController

- (void)setImage:(UIImage *)image;
- (void)setTitle:(NSString *)title;
- (void)startLoadingImage;
- (void)stopLoadingImage;
@end