//
//  LULoginViewController.m
//  Vimet
//
//  Created by Shuya Honda on 2014/06/26.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import "LULoginViewController.h"

#import <Accounts/Accounts.h>
#import "STTwitter.h"
#import "AFNetworking/AFNetworking.h"
#import "LUPlayerViewController.h"
#import "MBProgressHUD.h"
//#import <FacebookSDK/FacebookSDK.h>

static const NSString *facebookAppID = @"783088035082888";
static const NSString *twitterAPIKey = @"j8TOqyT8SSmwf51aaIQUiwwct";
static const NSString *twitterAPISecretKey = @"D5NW4ckh9fkewCegmk5Sm7t4LzSedGuiVkzb6wRq5NjrIUiD3R";

@interface LULoginViewController ()

@property (nonatomic) ACAccountStore *accountStore;
@property (weak, nonatomic) IBOutlet FBLoginView *loginView;

// for Twitter
@property NSString *accessToken;
@property NSString *accessTokenSecret;

@end

@implementation LULoginViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.accountStore == nil) {
        self.accountStore = [ACAccountStore new];
    }
    
    //FBLoginView *loginView = [[FBLoginView alloc] init];
    //loginView.frame = CGRectOffset(loginView.frame, (self.view.center.x - (loginView.frame.size.width / 2)), 5);

    //[self.view addSubview:loginView];
    /*
    self.loginView.readPermissions = @[@"public_profile", @"email"];
    self.loginView.delegate = self;
    */
    

}
// This method will be called when the user information has been fetched

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    NSLog(@"%@",user.name);
}

- (void)viewWillAppear:(BOOL)animated{
    
    if ([self isLogin]) {
        LUPlayerViewController *playerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerView"];
        [self.navigationController pushViewController:playerViewController animated:YES];
    }
    
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBAction
- (IBAction)buttonTapped:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email",
                            nil];
    
    // should close fbsession every time cause solving to repeat login and logout
    [FBSession.activeSession close];
    
    // make fbsession
    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {

        if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
            //
        } else if (!session.isOpen) {
            //
        } else {
            [[FBRequest requestForMe] startWithCompletionHandler:
             ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                 if (!error) {
                     NSLog(@"%@", session.accessTokenData.accessToken);
                     //ここで画面遷移
                     NSLog(@"認証に成功");
                     NSLog(@"access token = %@",session.accessToken); //アクセストークン
                     NSLog(@"user = %@",user.name); //ユーザー名
                     NSLog(@"user id = %@",user.id);

                     //  ここでログイン処理などをする
                     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                     // 送信するパラメータの組立
                     NSMutableDictionary *json = [NSMutableDictionary new];
                     [json setValue:@"facebook" forKey:@"provider"];
                     [json setValue:user.name forKey:@"nickname"];
                     
                     NSString *imageUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",user.name];
                     
                     [json setValue:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", user.name] forKey:@"image"];
                     [json setValue:imageUrl forKey:@"image"];
                     [json setValue:user.name forKey:@"external_id"];
                     
                     
                     NSMutableDictionary *credentials = [NSMutableDictionary new];
                     [credentials setValue:session.accessTokenData.accessToken forKey:@"token"];
                     [credentials setValue:@"100" forKey:@"expires_at"];
                     [json setValue:credentials forKey:@"credentials"];
                     
                     manager.requestSerializer = [AFJSONRequestSerializer serializer];
                     
                     NSLog(@"%@",json);
                     [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
                     
                     [manager POST:@"http://www4370uo.sakura.ne.jp/api/v1/users" parameters:json success:^(AFHTTPRequestOperation *operation, id responseObject){
                         NSLog(@"JSON: %@", responseObject);
                         
                         
                         //UserDefaultでexternal_idを保存
                         
                         //認証に使う anthentication_token を UserDefaultで保存
                         
                         NSDictionary *json = responseObject;
                         NSDictionary *data = [json objectForKey:@"data"];
                         NSString *authenticationToken = [data objectForKey:@"authentication_token"];
                         NSLog(@"save = %@",authenticationToken);
                         NSString *externalId = [data objectForKey:@"id"];
                         
                         NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                         [ud setObject:externalId forKey:@"UserId"];
                         [ud setObject:authenticationToken forKey:@"AuthenticationToken"];
                         
                         LUPlayerViewController *playerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerView"];
                         //                         [self presentViewController:playerViewController animated:YES completion:nil];
                         [self.navigationController pushViewController:playerViewController animated:YES];
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         
                     } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                         NSLog(@"error:%@", error);
                     }];

                     //LUPlayerViewController *playerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerView"];
                     //[self.navigationController pushViewController:playerViewController animated:YES];
                     
                 } else {
                     //認証に失敗
                     [[FBSession activeSession] closeAndClearTokenInformation];
                     [FBSession setActiveSession:nil];
                     NSLog(@"認証に失敗");
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                 }
             }];
        }
    }];
    [MBProgressHUD hideHUDForView:self.view animated:YES];

}

- (IBAction)tappedFacebookBtn:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    
    if (self.accountStore == nil) {
        self.accountStore = [ACAccountStore new];
    }

    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *options = @{ ACFacebookAppIdKey : facebookAppID, // MyFB = 494888840647657
                               ACFacebookAudienceKey : ACFacebookAudienceOnlyMe,
                               ACFacebookPermissionsKey : @[@"email"] };
    
    /*
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:facebookAppID,ACFacebookAppIdKey,
                                    [NSArray arrayWithObjects:@"basic_info",nil],ACFacebookPermissionsKey,
                                    ACFacebookAudienceEveryone,ACFacebookAudienceKey,nil
                                    ];
    */
    [self.accountStore
     requestAccessToAccountsWithType:accountType
     options:options
     completion:^(BOOL granted, NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (granted) {
                 // ユーザーがFacebookアカウントへのアクセスを許可した
                 //NSLog(@"アクセスが許可された");
                 //[self addFacebookPermission];
                 //return;
                 
                 NSArray *facebookAccounts = [self.accountStore accountsWithAccountType:accountType];
                 if (facebookAccounts.count > 0) {
                     ACAccount *facebookAccount = [facebookAccounts lastObject];
                     
                     // メールアドレスを取得する
                     NSString *email = [[facebookAccount valueForKey:@"properties"] objectForKey:@"ACUIDisplayUsername"];
                     
                     // アクセストークンを取得する
                     ACAccountCredential *facebookCredential = [facebookAccount credential];
                     NSString *accessToken = [facebookCredential oauthToken];
                     
                     NSLog(@"email:%@, token:%@", email, accessToken);
                     
                     //  ここでログイン処理などをする
                     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                     // 送信するパラメータの組立
                     NSMutableDictionary *json = [NSMutableDictionary new];
                     [json setValue:@"facebook" forKey:@"provider"];
                     [json setValue:facebookAccount.userFullName forKey:@"nickname"];
                     
                     NSString *imageUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [facebookAccount username]];
                     
                     [json setValue:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [facebookAccount username]] forKey:@"image"];
                     [json setValue:imageUrl forKey:@"image"];
                     [json setValue:facebookAccount.username forKey:@"external_id"];

                     
                     NSMutableDictionary *credentials = [NSMutableDictionary new];
                     [credentials setValue:accessToken forKey:@"token"];
                     [credentials setValue:@"100" forKey:@"expires_at"];
                     [json setValue:credentials forKey:@"credentials"];
                     
                     manager.requestSerializer = [AFJSONRequestSerializer serializer];

                     NSLog(@"%@",json);
                     [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
                     
                     [manager POST:@"http://www4370uo.sakura.ne.jp/api/v1/users" parameters:json success:^(AFHTTPRequestOperation *operation, id responseObject){
                         NSLog(@"JSON: %@", responseObject);
                         
                         
                         //UserDefaultでexternal_idを保存
                         
                         //認証に使う anthentication_token を UserDefaultで保存
                         
                         NSDictionary *json = responseObject;
                         NSDictionary *data = [json objectForKey:@"data"];
                         NSString *authenticationToken = [data objectForKey:@"authentication_token"];
                         NSLog(@"save = %@",authenticationToken);
//                         NSArray *accounts = [data objectForKey:@"accounts"];
//                         NSLog(@"%@",accounts);
//                         NSDictionary *account = [accounts firstObject];
//                         NSLog(@"%@",[accounts firstObject]);
//                         NSLog(@"%@",[account objectForKey:@"external_id"]);
//                         NSString *externalId = [account objectForKey:@"external_id"];
                         NSString *externalId = [data objectForKey:@"id"];
                        
                         NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                         [ud setObject:externalId forKey:@"UserId"];
                         [ud setObject:authenticationToken forKey:@"AuthenticationToken"];
                         
                         LUPlayerViewController *playerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerView"];
//                         [self presentViewController:playerViewController animated:YES completion:nil];
                         [self.navigationController pushViewController:playerViewController animated:YES];
                         [MBProgressHUD hideHUDForView:self.view animated:YES];

                         
                     } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                         NSLog(@"error:%@", error);
                     }];
                     
                     NSError *error;
                     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                                        options:NSJSONWritingPrettyPrinted
                                                                          error:&error];
                     NSLog(@"%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
                 }
             } else {
                 if([error code]== ACErrorAccountNotFound){
                     //  iOSに登録されているFacebookアカウントがありません。
                     NSLog(@"iOSにFacebookアカウントが登録されていません。設定→FacebookからFacebookアカウントを追加してください。");
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"確認"
                                                                     message:@"iOSにFacebookアカウントが登録されていません"
                                                                    delegate:nil
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:@"OK", nil
                                           ];
                     [alert show];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                 } else {
                     // ユーザーが許可しない
                     // 設定→Facebook→アカウントの使用許可するApp→YOUR_APPをオンにする必要がある
                     NSLog(@"Facebookが有効になっていません。");
                     NSLog(@"error code : %ld",[error code]);
                     NSArray *facebookAccounts = [self.accountStore accountsWithAccountType:accountType];
                     NSLog(@"facebook accouts count : %ld",facebookAccounts.count);
                     ACAccount *facebookAccount = [facebookAccounts lastObject];
                     
                     // メールアドレスを取得する
                     NSString *email = [[facebookAccount valueForKey:@"properties"] objectForKey:@"ACUIDisplayUsername"];
                     
                     // アクセストークンを取得する
                     ACAccountCredential *facebookCredential = [facebookAccount credential];
                     NSString *accessToken = [facebookCredential oauthToken];
                     
                     NSLog(@"email:%@, token:%@", email, accessToken);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"確認"
                                                                     message:@"Vimetのアカウント使用許可をオンにしてください。"
                                                                    delegate:nil
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:@"OK", nil
                                           ];
                     [alert show];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                 }
             }
         });
     }];
}


-(void)addFacebookPermission{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *facebookType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *options = @{ACFacebookAppIdKey : facebookAppID,
                              ACFacebookPermissionsKey : @[@"publish_stream"],
                              ACFacebookAudienceKey : ACFacebookAudienceEveryone};
    
    [accountStore requestAccessToAccountsWithType:facebookType options:options completion:^(BOOL granted, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(granted){
                
                NSLog(@"認証できました");
                NSArray *accounts = [accountStore
                                     accountsWithAccountType:facebookType];
                ACAccount *account = [accounts lastObject];
                
                NSString *userName = [account valueForKey:@"username"];
            }else{
                //認証失敗
            }
        });
    }];
}

-(NSString*)getJsonStringByDictionary:(NSDictionary*)dictionary{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (IBAction)tappedTwitterBtn:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIWithOAuthConsumerName:nil
                                                              consumerKey:@"j8TOqyT8SSmwf51aaIQUiwwct"
                                                           consumerSecret:@"D5NW4ckh9fkewCegmk5Sm7t4LzSedGuiVkzb6wRq5NjrIUiD3R"];
    
    [twitter postReverseOAuthTokenRequest:^(NSString *authenticationHeader) {
        
        STTwitterAPI *twitterAPIOS = [STTwitterAPI twitterAPIOSWithFirstAccount];
        
        [twitterAPIOS verifyCredentialsWithSuccessBlock:^(NSString *username) {
            
            [twitterAPIOS postReverseAuthAccessTokenWithAuthenticationHeader:authenticationHeader
                                                                successBlock:^(NSString *oAuthToken,
                                                                               NSString *oAuthTokenSecret,
                                                                               NSString *userID,
                                                                               NSString *screenName) {
                                                                    self.accessToken = oAuthToken;
                                                                    self.accessTokenSecret = oAuthTokenSecret;
                                                                    NSLog(@"Token %@ secret %@",oAuthToken,oAuthTokenSecret);
                                                                    
                                                                    
                                                                    NSArray *twitterAccounts = [self.accountStore accountsWithAccountType:accountType];
                                                                    NSLog(@"%lu",(unsigned long)twitterAccounts.count);
                                                                    
                                                                    
                                                                    ACAccount *twitterAccount = [twitterAccounts lastObject];
                                                                    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                                                                    NSMutableDictionary *json = [NSMutableDictionary new];
                                                                    [json setValue:@"twitter" forKey:@"provider"];
                                                                    [json setValue:screenName forKey:@"nickname"];
                                                                    
                                                                    [json setValue:[NSString stringWithFormat:@"https://twitter.com/api/users/profile_image/%@?size=original", [twitterAccount username]] forKey:@"image"];
                                                                    [json setValue:userID forKey:@"external_id"];
                                                                    
                                                                    NSMutableDictionary *credentials = [NSMutableDictionary new];
                                                                    [credentials setValue:oAuthToken forKey:@"oauth_token"];
                                                                    [credentials setValue:oAuthTokenSecret forKey:@"oauth_token_secret"];
                                                                    [json setValue:credentials forKey:@"credentials"];
                                                                    
                                                                    manager.requestSerializer = [AFJSONRequestSerializer serializer];
                                                                    
                                                                    NSLog(@"%@",json);
                                                                    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
                                                                    [manager POST:@"http://www4370uo.sakura.ne.jp/api/v1/users" parameters:json success:^(AFHTTPRequestOperation *operation, id responseObject){
                                                                        NSLog(@"JSON: %@", responseObject);
                                                                        
                                                        
                                                                        //UserDefaultでexternal_idを保存
                                                                        
                                                                        //認証に使う anthentication_token を UserDefaultで保存
                                                                        
                                                                        NSDictionary *json = responseObject;
                                                                        NSDictionary *data = [json objectForKey:@"data"];
                                                                        NSString *authenticationToken = [data objectForKey:@"authentication_token"];
                                                                        NSLog(@"save = %@",authenticationToken);
                                                                        NSString *externalId = [data objectForKey:@"id"];
                                                                        
                                                                        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                                        [ud setObject:externalId forKey:@"UserId"];
                                                                        [ud setObject:authenticationToken forKey:@"AuthenticationToken"];
                                                                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                                                                        
                                                                        LUPlayerViewController *playerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerView"];
                                                                        //                         [self presentViewController:playerViewController animated:YES completion:nil];
                                                                        [self.navigationController pushViewController:playerViewController animated:YES];

                                                                    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                                                                        NSLog(@"error:%@", error);
                                                                    }];
                                                                    
                                                                    NSError *error;
                                                                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                                                                                       options:NSJSONWritingPrettyPrinted
                                                                                                                         error:&error];
                                                                    NSLog(@"%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);

                                                                    
                                                                    
                                                                    
                                                                } errorBlock:^(NSError *error) {
                                                                    NSLog(@"error %@",[error description]);
                                                                    
                                                                }];
            
        } errorBlock:^(NSError *error) {
            NSLog(@"error1");
            if(error.code == 0){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"確認"
                                                                message:@"iOSにTwitterアカウントが登録されていません"
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK", nil
                                      ];
                [alert show];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"確認"
                                                                message:@"Vimetのアカウント使用許可をオンにしてください。"
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK", nil
                                      ];
                [alert show];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            NSLog(@"error %@",[error description]);
        }];
        
    } errorBlock:^(NSError *error) {
        NSLog(@"error2");
        NSLog(@"error %@",[error description]);
    }];
    
    
}

#pragma mark -

- (BOOL)isLogin
{
    // 認証キーが保存されているかどうかで判定 => 都度サーバに問い合わせる必要あり？
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud stringForKey:@"AuthenticationToken"] != nil) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Status Bar

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

@end