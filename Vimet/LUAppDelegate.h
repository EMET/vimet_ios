//
//  LUAppDelegate.h
//  Vimet
//
//  Created by Shuya Honda on 2014/06/17.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
