//
//  main.m
//  Vimet
//
//  Created by Shuya Honda on 2014/06/17.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LUAppDelegate class]));
    }
}
