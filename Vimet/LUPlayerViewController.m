//
//  LUPlayerViewController.m
//  Vimet
//
//  Created by Shuya Honda on 2014/07/04.
//  Copyright (c) 2014年 Shuya Honda. All rights reserved.
//

#import "LUPlayerViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import <Social/Social.h>

#import "LUNextLoadingViewController.h"
#import "LUSearchViewController.h"
#import "LMMediaItem.h"
#import "LMMediaItemQueueManager.h"
#import "AFNetworking/AFNetworking.h"

static NSString* const apiBaseUrl = @"http://www4370uo.sakura.ne.jp/api/v1/";
static NSString* const apiVideos = @"videos";
static NSString* const apiUsers = @"users";

@interface LUPlayerViewController ()

@property (weak, nonatomic) IBOutlet UIView     *titleBar;
@property (weak, nonatomic) IBOutlet UIView     *likeView;
@property (weak, nonatomic) IBOutlet UIView     *ctrlView;
@property (weak, nonatomic) IBOutlet UIView     *dislikeView;
@property (weak, nonatomic) IBOutlet UIButton   *videoTitleBtn;
@property (weak, nonatomic) IBOutlet UIButton   *playBtn;
@property (weak, nonatomic) IBOutlet UISlider   *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UILabel    *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel    *endTimeLabel;

@property (weak, nonatomic) IBOutlet UIButton   *LikeBtn;
@property (weak, nonatomic) IBOutlet UIButton   *DislikeBtn;

@property LUNextLoadingViewController           *nextLoadingViewController;
@property LUSearchViewController                *searchViewController;
@property LMMediaPlayerView                     *playerView;
@property MPMediaPlaylist                       *playList;
@property NSArray                               *videoInfos;
@property NSUInteger                            playingIndex;
@property CGPoint                               touchBeganPoint;
@property BOOL                                  isVisibleCtrlView;
@property BOOL                                  isHiddenStatusBar;
@property BOOL                                  isMovingRigthToLeft;
@property BOOL                                  isMovingLeftToRight;
@property BOOL                                  isOnceMoved;

@end

@implementation LUPlayerViewController

static float const kLoadTime = 3.5;

#pragma mark - View Cycle

- (void)viewDidLoad{
    self.playingIndex = 0;
    
    // StoryboardからViewControllerを取得
    self.nextLoadingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NextLoadingView"];
    self.searchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
    
    // navigationController
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    // playerView
    self.playerView = [LMMediaPlayerView sharedPlayerView];
    self.playerView.delegate = self;
    self.playerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.playerView.currentTimeSlider = self.currentTimeSlider;
    [self.playerView updateCurrentSlider];
    [self.playerView setPlaybackTimeLabel:self.startTimeLabel];
    [self.view addSubview:self.playerView];

    // bringSubviewToFront
    UIView *visibleChangeBtn = [self.view viewWithTag:10];
    [self.view bringSubviewToFront:visibleChangeBtn];
    [self.view bringSubviewToFront:self.titleBar];
    [self.view bringSubviewToFront:self.ctrlView];
    [self.view bringSubviewToFront:self.dislikeView];
    [self.view bringSubviewToFront:self.likeView];
    
    self.isVisibleCtrlView = YES;
    self.isHiddenStatusBar = NO;
    
    // 動画を取得 -> self.videoInfos
    NSURL *url = [NSURL URLWithString:@"http://www4370uo.sakura.ne.jp/api/v1/videos"];
    NSError *error = nil;
    NSData *jsonData = (NSData *)[NSData dataWithContentsOfURL:url options:kNilOptions error:&error];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:(NSData *)jsonData options:NSJSONReadingAllowFragments error:&error];
    self.videoInfos = [json objectForKey:@"data"];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"%@",[ud stringForKey:@"AuthenticationToken"]);
    NSString *requestUrl = [NSString stringWithFormat:@"%@/recommend?user_id=%@",url,[ud stringForKey:@"UserId"]];
    
    [manager GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        NSDictionary *json = responseObject;
        self.videoInfos = [json objectForKey:@"data"];
        
        NSDictionary *dict = (NSDictionary *)[self.videoInfos firstObject];
        NSString *videoId = (NSString *)[dict objectForKey:@"id"];
        NSURL *videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",url,videoId]];
        NSLog(@"videoUrl = %@",videoUrl);
        
        [manager GET:[NSString stringWithFormat:@"%@/%@",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *json = responseObject;
            NSDictionary *data = (NSDictionary *)[json objectForKey:@"data"];
            NSString *streamUrl = [data objectForKey:@"stream_url"];
            NSLog(@"url = %@",streamUrl);
            //NSLog(@"nsurl = %@",[NSURL URLWithString:[streamUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] );
            streamUrl = [streamUrl stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
            LMMediaItem *mediaItem = [[LMMediaItem alloc] initWithInfo:@{
                                                                         LMMediaItemInfoURLKey:[NSURL URLWithString:streamUrl],
                                                                         LMMediaItemInfoURLKey:[NSNumber numberWithInteger:LMMediaItemContentTypeVideo]
                                                                         }
                                      ];
            [self.playerView.mediaPlayer addMedia:mediaItem];
            [self.playerView.mediaPlayer play];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.playerView.mediaPlayer setRepeatMode:LMMediaRepeatModeNone];
    [self.playBtn setImage:[UIImage imageNamed:@"video-pause"] forState:UIControlStateNormal];
}

#pragma mark - MediaPlayer Delegate

- (BOOL)mediaPlayerViewWillStartPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media{
    [self setVideoTitle];
    [self setVideoDuration];
    //[self showBar];
    [self.nextLoadingViewController.view removeFromSuperview];
    [self.playBtn setImage:[UIImage imageNamed:@"video-pause"] forState:UIControlStateNormal];
    
    //タイマーを停止する
//    [NSTimer ]
    return YES;
}

- (void)mediaPlayerViewDidFinishPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media{
    //if (!self.isVisibleCtrlView) {
    //    [self showBar];
    //}
    [self.playBtn setImage:[UIImage imageNamed:@"video-play"] forState:UIControlStateNormal];
    
    [NSTimer scheduledTimerWithTimeInterval:0.0f
                                     target:self
                                   selector:@selector(autoLoadNextVideo)
                                   userInfo:nil
                                    repeats:NO
     ];
    
}

- (void)mediaPlayerViewWillChangeState:(LMMediaPlayerView *)playerView state:(LMMediaPlaybackState)state{
    
}

- (void)reverseUserInterfaceHidden:(BOOL)hidden{
    if (self.isVisibleCtrlView) {
        [self hideBar];
    } else {
        [self showBar];
        
        [NSTimer scheduledTimerWithTimeInterval:3.0f
                                         target:self
                                       selector:@selector(autoHideBar)
                                       userInfo:nil
                                        repeats:NO
         ];
    };
}

#pragma mark - IBAction

- (IBAction)tappedTitleBtn:(id)sender {
    [[UIApplication sharedApplication]
     openURL:[NSURL URLWithString:[(NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex] valueForKey:@"media_url"]]];
}

- (IBAction)tappedFacebookBtn:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        NSString *serviceType = SLServiceTypeFacebook;
        SLComposeViewController *composeCtl = [SLComposeViewController composeViewControllerForServiceType:serviceType];
        
        NSString *initialTitle = [(NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex] valueForKey:@"title"];
        NSString *initialUrl = [(NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex] valueForKey:@"original_url"];
        
        [composeCtl setInitialText:[NSString stringWithFormat:@"%@ %@",initialTitle,initialUrl]];

        [composeCtl setCompletionHandler:^(SLComposeViewControllerResult result) {
            if (result == SLComposeViewControllerResultDone) {
                //投稿成功時の処理
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
                [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
                
                NSString *requestUrl = [NSString stringWithFormat:@"%@/videos/%d/share",apiBaseUrl,[self getVideoId]];
                
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
                [dict setValue:@"facebook" forKey:@"provider"];
                
                [manager POST:requestUrl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject){
                    NSLog(@"%@",responseObject);
                } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                    NSLog(@"error:%@", error);
                }];
            }
        }];
        [self presentViewController:composeCtl animated:YES completion:nil];
    }
}

- (IBAction)tappedTwitterBtn:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        NSString *serviceType = SLServiceTypeTwitter;
        SLComposeViewController *composeCtl = [SLComposeViewController composeViewControllerForServiceType:serviceType];
        
        NSString *initialTitle = [(NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex] valueForKey:@"title"];
        NSString *initialUrl = [(NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex] valueForKey:@"original_url"];
        
        [composeCtl setInitialText:[NSString stringWithFormat:@"%@ %@",initialTitle,initialUrl]];
        
        [composeCtl setCompletionHandler:^(SLComposeViewControllerResult result) {
            if (result == SLComposeViewControllerResultDone) {
                //投稿成功時の処理
                NSLog(@"投稿成功");
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
                [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
                
                NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/share",apiBaseUrl,[self getVideoId]];
                NSLog(@"%@",[self getVideoId]);
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
                [dict setValue:@"twitter" forKey:@"provider"];
                
                NSLog(@"%@",dict);
                
                [manager POST:requestUrl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject){
                    NSLog(@"%@",responseObject);
                } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                    NSLog(@"error:%@", error);
                }];
            } else {
                NSLog(@"投稿失敗");
            }
        }];
        [self presentViewController:composeCtl animated:YES completion:nil];
    }
}

- (IBAction)tappedLikeBtn:(id)sender {
    //[self hideBar];
    [self nextLoadRightToLeft];
}

- (IBAction)tappedDislikeBtn:(id)sender {
    //[self hideBar];
    [self nextLoadLeftToRight];
}

- (IBAction)tappedPlayButton:(id)sender {
    if ([self.playerView.mediaPlayer playbackState] == LMMediaPlaybackStatePlaying) {
        [self.playerView.mediaPlayer pause];
        [self.playBtn setImage:[UIImage imageNamed:@"video-play"] forState:UIControlStateNormal];
    } else if ([self.playerView.mediaPlayer playbackState] == LMMediaPlaybackStatePaused){
        [self.playerView.mediaPlayer play];
        [self.playBtn setImage:[UIImage imageNamed:@"video-pause"] forState:UIControlStateNormal];
    } else if ([self.playerView.mediaPlayer playbackState] == LMMediaPlaybackStateStopped){
        [self.playerView.mediaPlayer playAtIndex:self.playingIndex];
        [self.playBtn setImage:[UIImage imageNamed:@"video-pause"] forState:UIControlStateNormal];
    }
}

# pragma mark - TouchEvent

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    self.touchBeganPoint = [[touches anyObject] locationInView:self.view];
    //self.nextLoadingViewController.view.alpha = 0;
    self.nextLoadingViewController.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.width);
    [self.view addSubview:self.nextLoadingViewController.view];
    self.isOnceMoved = NO;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint touch = [[touches anyObject] locationInView:self.view];
    
    if (!self.isOnceMoved) {
        if (touch.x - self.touchBeganPoint.x > 0) {
            self.isMovingLeftToRight = YES;
            self.isMovingRigthToLeft = NO;
            self.nextLoadingViewController.view.frame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
        } else {
            self.isMovingRigthToLeft = YES;
            self.isMovingLeftToRight = NO;
            self.nextLoadingViewController.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
        self.isOnceMoved = YES;
        self.nextLoadingViewController.view.alpha = 1;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.playingIndex++;
            NSDictionary *video = [self.videoInfos objectAtIndex:self.playingIndex];
            NSString *imgUrl = (NSString *)[video objectForKey:@"image_url"];
            NSString *nextTitle = (NSString *)[video objectForKey:@"title"];
            UIImage *nextThumb = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]]];
            [self.nextLoadingViewController setImage:nextThumb];
            [self.nextLoadingViewController setTitle:nextTitle];
            self.playingIndex--;
        });
    }
    
    if (self.isMovingRigthToLeft) {
        self.nextLoadingViewController.view.frame = CGRectMake(self.view.frame.size.width + (touch.x - self.touchBeganPoint.x),0,self.view.frame.size.width,self.view.frame.size.height);
    } else if (self.isMovingLeftToRight){
        self.nextLoadingViewController.view.frame = CGRectMake(-self.view.frame.size.width + (touch.x - self.touchBeganPoint.x),0,self.view.frame.size.width,self.view.frame.size.height);
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (self.isMovingRigthToLeft) {
        if (self.nextLoadingViewController.view.frame.origin.x <= self.view.frame.size.width/1.4) {
            //アニメーションで左まで
            NSDictionary *dict = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex+1];
            NSString *videoId = (NSString *)[dict objectForKey:@"id"];
            NSURL *url = [NSURL URLWithString:@"http://www4370uo.sakura.ne.jp/api/v1/videos"];
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
            
            [manager GET:[NSString stringWithFormat:@"%@/%@",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSDictionary *json = responseObject;
                NSDictionary *data = (NSDictionary *)[json objectForKey:@"data"];
                NSString *streamUrl = [data objectForKey:@"stream_url"];
                
                //NSLog(@"stream url = %@",streamUrl);
                //NSLog(@"escaped url = %@",[streamUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
                //NSLog(@"remove percent = %@",[streamUrl stringByRemovingPercentEncoding]);
                
                streamUrl = [streamUrl stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
                
                LMMediaItem *mediaItem = [[LMMediaItem alloc] initWithInfo:@{
                                                                             LMMediaItemInfoURLKey:[NSURL URLWithString:streamUrl],
                                                                             LMMediaItemInfoURLKey:[NSNumber numberWithInteger:LMMediaItemContentTypeVideo]
                                                                             }
                                          ];
                
                [self.playerView.mediaPlayer addMedia:mediaItem];
            }failure:^(AFHTTPRequestOperation *operation,NSError *error){
                NSLog(@"error:%@", error);
            }];
            
            
            NSDictionary *dict2 = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex];
            NSString *videoId2 = (NSString *)[dict2 objectForKey:@"id"];
            NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/rate",apiBaseUrl,videoId2];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *lateDict = [NSMutableDictionary new];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            
            [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
            [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
            
            NSLog(@"%@",[ud stringForKey:@"AuthenticationToken"]);
            
            [lateDict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
            [lateDict setValue:@"dislike" forKey:@"score"];
            [lateDict setValue:@"1" forKey:@"viewing_time"];
            NSLog(@"%@",lateDict);
            
            NSLog(@"dislike");
            [manager POST:requestUrl parameters:lateDict success:^(AFHTTPRequestOperation *operation, id responseObject){
                NSLog(@"%@",responseObject);
            } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                NSLog(@"error:%@", error);
            }];

        
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^(void){
                                 self.nextLoadingViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                             }completion:^(BOOL finished){
                                 [self.playerView.mediaPlayer pause];
//                                 [NSThread sleepForTimeInterval:kLoadTime];
                                 
                                 //loading Start
                                 [NSTimer scheduledTimerWithTimeInterval:kLoadTime
                                                                  target:self
                                                                selector:@selector(timerAfter)
                                                                userInfo:nil
                                                                 repeats:NO
                                  ];
                                 [self.nextLoadingViewController startLoadingImage];
                                 
                                 self.playingIndex++;
                             }];
        } else {
            //アニメーションで右まで元に戻す
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^(void){
                                 self.nextLoadingViewController.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
                             }completion:^(BOOL finished){
                                 [self setNeedsStatusBarAppearanceUpdate];
                             }];
        }
    } else if (self.isMovingLeftToRight){
        if (self.nextLoadingViewController.view.frame.origin.x >= -self.view.frame.size.width/1.4) {
            //アニメーションで右まで
            NSURL *url = [NSURL URLWithString:@"http://www4370uo.sakura.ne.jp/api/v1/videos"];
            
            NSDictionary *dict = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex+1];
            NSString *videoId = (NSString *)[dict objectForKey:@"id"];
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
            
            [manager GET:[NSString stringWithFormat:@"%@/%@",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSDictionary *json = responseObject;
                NSDictionary *data = (NSDictionary *)[json objectForKey:@"data"];
                NSString *streamUrl = [data objectForKey:@"stream_url"];
                
                //NSLog(@"stream url = %@",streamUrl);
                //NSLog(@"escaped url = %@",[streamUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
                //NSLog(@"remove percent = %@",[streamUrl stringByRemovingPercentEncoding]);
                
                streamUrl = [streamUrl stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
                
                LMMediaItem *mediaItem = [[LMMediaItem alloc] initWithInfo:@{
                                                                             LMMediaItemInfoURLKey:[NSURL URLWithString:streamUrl],
                                                                             LMMediaItemInfoURLKey:[NSNumber numberWithInteger:LMMediaItemContentTypeVideo]
                                                                             }
                                          ];
                
                [self.playerView.mediaPlayer addMedia:mediaItem];
            }failure:^(AFHTTPRequestOperation *operation,NSError *error){
                NSLog(@"error:%@", error);
            }];
            
            NSDictionary *dict2 = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex];
            NSString *videoId2 = (NSString *)[dict2 objectForKey:@"id"];
            NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/rate",apiBaseUrl,videoId2];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *lateDict = [NSMutableDictionary new];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            
            [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
            [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
            
            NSLog(@"%@",[ud stringForKey:@"AuthenticationToken"]);
            
            [lateDict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
            [lateDict setValue:@"like" forKey:@"score"];
            [lateDict setValue:@"1" forKey:@"viewing_time"];
            NSLog(@"%@",lateDict);
            
            NSLog(@"like");
            [manager POST:requestUrl parameters:lateDict success:^(AFHTTPRequestOperation *operation, id responseObject){
                NSLog(@"%@",responseObject);
            } failure:^(AFHTTPRequestOperation *operation,NSError *error){
                NSLog(@"error:%@", error);
            }];

            
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^(void){
                                 self.nextLoadingViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                             }completion:^(BOOL finished){
                                 [self.playerView.mediaPlayer pause];
//                                 [NSThread sleepForTimeInterval:kLoadTime];
                                 [NSTimer scheduledTimerWithTimeInterval:kLoadTime
                                                                  target:self
                                                                selector:@selector(timerAfter)
                                                                userInfo:nil
                                                                 repeats:NO
                                  ];
                                 [self.nextLoadingViewController startLoadingImage];
                                 
                                 self.playingIndex++;
//                                 [self.playerView.mediaPlayer playAtIndex:self.playingIndex];
                             }];
        } else {
            //アニメーションで左まで元に戻す
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^(void){
                                 self.nextLoadingViewController.view.frame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
                             }completion:^(BOOL finished){
                                 [self setNeedsStatusBarAppearanceUpdate];
                             }];
        }
    }
}


#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)prefersStatusBarHidden
{
    return self.isHiddenStatusBar;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)nextLoadRightToLeft{
    [self.playerView.mediaPlayer pause];

    UIImageView *likeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"like"]];
    likeImage.frame = CGRectMake(self.view.frame.size.width/2 - 25, self.view.frame.size.height/2 - 25, 50, 50);
    [likeImage sizeToFit];
    likeImage.alpha = 1;
    [self.view addSubview:likeImage];
    
    [UIView animateWithDuration:0.1f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void){
                         CGAffineTransform scale = CGAffineTransformMakeScale(2.0, 2.0);
                         likeImage.transform = scale;
                     } completion:^(BOOL finished){
                         
                     }];
    
    [UIView animateWithDuration:0.3f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void){
                         likeImage.alpha = 0;
                     } completion:^(BOOL finished){
                         [likeImage removeFromSuperview];
                         
                     }];
    
    [self.view addSubview:self.nextLoadingViewController.view];
    self.nextLoadingViewController.view.frame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    self.playingIndex++;
    
    NSDictionary *video = [self.videoInfos objectAtIndex:self.playingIndex];
    NSString *imgUrl = (NSString *)[video objectForKey:@"image_url"];
    NSString *nextTitle = (NSString *)[video objectForKey:@"title"];
    UIImage *nextThumb = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]]];
    [self.nextLoadingViewController setImage:nextThumb];
    [self.nextLoadingViewController setTitle:nextTitle];
    
    self.playingIndex--;
    NSURL *url = [NSURL URLWithString:@"http://www4370uo.sakura.ne.jp/api/v1/videos"];
    
    NSDictionary *dict = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex+1];
    NSString *videoId = (NSString *)[dict objectForKey:@"id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];

    [manager GET:[NSString stringWithFormat:@"%@/%@",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *json = responseObject;
        NSDictionary *data = (NSDictionary *)[json objectForKey:@"data"];
        NSString *streamUrl = [data objectForKey:@"stream_url"];
        
        //NSLog(@"stream url = %@",streamUrl);
        //NSLog(@"escaped url = %@",[streamUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        //NSLog(@"remove percent = %@",[streamUrl stringByRemovingPercentEncoding]);

        streamUrl = [streamUrl stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
        LMMediaItem *mediaItem = [[LMMediaItem alloc] initWithInfo:@{
                                                                     LMMediaItemInfoURLKey:[NSURL URLWithString:streamUrl],

                                                                     LMMediaItemInfoURLKey:[NSNumber numberWithInteger:LMMediaItemContentTypeVideo]
                                                                     }
                                  ];
        
        [self.playerView.mediaPlayer addMedia:mediaItem];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];
    
    
    [UIView animateWithDuration:0.3f
                          delay:0.6f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^(void){
                         self.nextLoadingViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
//                         [NSThread sleepForTimeInterval:kLoadTime];
                         [NSTimer scheduledTimerWithTimeInterval:kLoadTime
                                                          target:self
                                                        selector:@selector(timerAfter)
                                                        userInfo:nil
                                                         repeats:NO
                          ];
                         [self.nextLoadingViewController startLoadingImage];
                         
                         self.playingIndex++;

//                         [self.playerView.mediaPlayer playAtIndex:self.playingIndex];
                     }];
    
    
    // Like APIを叩く
    NSDictionary *dict2 = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex];
    NSString *videoId2 = (NSString *)[dict2 objectForKey:@"id"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/rate",apiBaseUrl,videoId2];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *lateDict = [NSMutableDictionary new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];

    NSLog(@"%@",[ud stringForKey:@"AuthenticationToken"]);
    
    [lateDict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
    [lateDict setValue:@"like" forKey:@"score"];
    [lateDict setValue:@"1" forKey:@"viewing_time"];
    NSLog(@"%@",lateDict);
    
    NSLog(@"like");
    [manager POST:requestUrl parameters:lateDict success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];
}


- (void)nextLoadLeftToRight{
    [self.playerView.mediaPlayer pause];

    UIImageView *likeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dislike"]];
    likeImage.frame = CGRectMake(self.view.frame.size.width/2 - 25, self.view.frame.size.height/2 - 25, 50, 50);
    [likeImage sizeToFit];
    likeImage.alpha = 1;
    [self.view addSubview:likeImage];
    
    [UIView animateWithDuration:0.1f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^(void){
                         CGAffineTransform scale = CGAffineTransformMakeScale(2.0, 2.0);
                         likeImage.transform = scale;
                     } completion:^(BOOL finished){
                         
                     }];
    
    [UIView animateWithDuration:0.3f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^(void){
                         likeImage.alpha = 0;
                     } completion:^(BOOL finished){
                         [likeImage removeFromSuperview];
                     }];
    
    [self.view addSubview:self.nextLoadingViewController.view];
    self.nextLoadingViewController.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.playingIndex++;
    
    NSDictionary *video = [self.videoInfos objectAtIndex:self.playingIndex];
    NSString *imgUrl = (NSString *)[video objectForKey:@"image_url"];
    NSString *nextTitle = (NSString *)[video objectForKey:@"title"];
    UIImage *nextThumb = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]]];
    [self.nextLoadingViewController setImage:nextThumb];
    [self.nextLoadingViewController setTitle:nextTitle];
    self.playingIndex--;
    
    
    NSDictionary *dict = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex+1];
    NSString *videoId = (NSString *)[dict objectForKey:@"id"];

    NSURL *url = [NSURL URLWithString:@"http://www4370uo.sakura.ne.jp/api/v1/videos"];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
    
    [manager GET:[NSString stringWithFormat:@"%@/%@",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *json = responseObject;
        NSDictionary *data = (NSDictionary *)[json objectForKey:@"data"];
        NSString *streamUrl = [data objectForKey:@"stream_url"];
        
        //NSLog(@"stream url = %@",streamUrl);
        //NSLog(@"escaped url = %@",[streamUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        //NSLog(@"remove percent = %@",[streamUrl stringByRemovingPercentEncoding]);
        
        streamUrl = [streamUrl stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];

        LMMediaItem *mediaItem = [[LMMediaItem alloc] initWithInfo:@{
                                                                     LMMediaItemInfoURLKey:[NSURL URLWithString:streamUrl],
                                                                     LMMediaItemInfoURLKey:[NSNumber numberWithInteger:LMMediaItemContentTypeVideo]
                                                                     }
                                  ];
        
        [self.playerView.mediaPlayer addMedia:mediaItem];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];
    
    /*
    //Rate Like
    NSLog(@"rate like video ID = %@",[NSString stringWithFormat:@"%@/%@/rate",url,videoId]);
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"AuthenticationToken"]  forKey:@"Authorization"];
    [manager POST:[NSString stringWithFormat:@"%@/%@/rate",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
     */
    
    [UIView animateWithDuration:0.3f
                          delay:0.6f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^(void){
                         self.nextLoadingViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
//                         [NSThread sleepForTimeInterval:kLoadTime];
                         [NSTimer scheduledTimerWithTimeInterval:kLoadTime
                                                          target:self
                                                        selector:@selector(timerAfter)
                                                        userInfo:nil
                                                         repeats:NO
                          ];
                         [self.nextLoadingViewController startLoadingImage];
                         
                         self.playingIndex++;
//                         [self.playerView.mediaPlayer playAtIndex:self.playingIndex];
                     }];
    
    //[self rateVideo:YES];
    // DiLike APIを叩く
    NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/rate",apiBaseUrl,videoId];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *lateDict = [NSMutableDictionary new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];

    NSLog(@"%@",[ud stringForKey:@"AuthenticationToken"]);
    [lateDict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
    [lateDict setValue:@"dislike" forKey:@"score"];
    [lateDict setValue:@"1" forKey:@"viewing_time"];
    NSLog(@"%@",lateDict);
    
    NSLog(@"dislike");
    [manager POST:requestUrl parameters:lateDict success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];

}

- (void)autoLoadNextVideo{
    NSLog(@"call --- autoLoadNextVideo");
    [self.view addSubview:self.nextLoadingViewController.view];
    self.nextLoadingViewController.view.frame = CGRectMake( -self.view.frame.size.width,0, self.view.frame.size.width, self.view.frame.size.height);
    self.playingIndex++;
    
    NSDictionary *video = [self.videoInfos objectAtIndex:self.playingIndex];
    NSString *imgUrl = (NSString *)[video objectForKey:@"image_url"];
    NSString *nextTitle = (NSString *)[video objectForKey:@"title"];
    UIImage *nextThumb = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]]];
    [self.nextLoadingViewController setImage:nextThumb];
    [self.nextLoadingViewController setTitle:nextTitle];
    self.playingIndex--;
    
    
    NSDictionary *dict = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex+1];
    NSString *videoId = (NSString *)[dict objectForKey:@"id"];
    
    NSURL *url = [NSURL URLWithString:@"http://www4370uo.sakura.ne.jp/api/v1/videos"];

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
    
    [manager GET:[NSString stringWithFormat:@"%@/%@",url,videoId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *json = responseObject;
        NSDictionary *data = (NSDictionary *)[json objectForKey:@"data"];
        NSString *streamUrl = [data objectForKey:@"stream_url"];
        
        //NSLog(@"stream url = %@",streamUrl);
        //NSLog(@"escaped url = %@",[streamUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        //NSLog(@"remove percent = %@",[streamUrl stringByRemovingPercentEncoding]);
        
        streamUrl = [streamUrl stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
        
        LMMediaItem *mediaItem = [[LMMediaItem alloc] initWithInfo:@{
                                                                     LMMediaItemInfoURLKey:[NSURL URLWithString:streamUrl],
                                                                     LMMediaItemInfoURLKey:[NSNumber numberWithInteger:LMMediaItemContentTypeVideo]
                                                                     }
                                  ];
        
        [self.playerView.mediaPlayer addMedia:mediaItem];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];

    [UIView animateWithDuration:0.3f
                          delay:0.6f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^(void){
                         self.nextLoadingViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
//                         [NSThread sleepForTimeInterval:kLoadTime];
                         [NSTimer scheduledTimerWithTimeInterval:kLoadTime
                                                          target:self
                                                        selector:@selector(timerAfter)
                                                        userInfo:nil
                                                         repeats:NO
                          ];
                         [self.nextLoadingViewController startLoadingImage];
                         
                         self.playingIndex++;
//                         [self.playerView.mediaPlayer playAtIndex:self.playingIndex];
                     }];

    
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/rate",apiBaseUrl,videoId];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *lateDict = [NSMutableDictionary new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
    
    NSLog(@"%@",[ud stringForKey:@"AuthenticationToken"]);
    [lateDict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
    [lateDict setValue:@"seen" forKey:@"score"];
    [lateDict setValue:@"1" forKey:@"viewing_time"];
    NSLog(@"%@",lateDict);
    
    NSLog(@"seen");
    [manager POST:requestUrl parameters:lateDict success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];

}

- (void)hideBar{
    self.isHiddenStatusBar = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^(void) {
                         [self.titleBar setAlpha:0];
                         [self.dislikeView setAlpha:0];
                         [self.likeView setAlpha:0];
                         [self.ctrlView setAlpha:0];
                     } completion:^(BOOL finished){
                         self.isVisibleCtrlView = NO;
                     }];
}

- (void)showBar{
    self.isHiddenStatusBar = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^(void) {
                         [self.titleBar setAlpha:1];
                         [self.dislikeView setAlpha:1];
                         [self.likeView setAlpha:1];
                         [self.ctrlView setAlpha:1];
                     } completion:^(BOOL finished){
                         self.isVisibleCtrlView = YES;
                     }];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setVideoTitle{
    NSString *str;
    
    str = [(NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex] valueForKey:@"title"];
    
    NSMutableAttributedString *attrStr;
    attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSDictionary *attributes = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    [attrStr addAttributes:attributes range:NSMakeRange(0, [attrStr length])];
    [self.videoTitleBtn setAttributedTitle:attrStr forState:UIControlStateNormal];
}

- (void)setVideoDuration{
    //playingIndexとvideoInfosからDurationを設定
    NSDictionary *video = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex];
    NSUInteger duration = [[video objectForKey:@"duration"] intValue];
    
    NSUInteger minute = (NSUInteger)duration/60;
    NSUInteger second = (NSUInteger)duration%60;
    NSLog(@"%lu,%lu,%lu",duration,minute,second);
    
    if (minute/10 == 0 && second/10 == 0) {
        self.endTimeLabel.text = [NSString stringWithFormat:@"0%lu:0%lu",minute,second];
    } else if (minute/10 == 0 && second/10 != 0){
        self.endTimeLabel.text = [NSString stringWithFormat:@"0%lu:%lu",minute,second];
    } else if (minute/10 != 0 && second/10 == 0){
        self.endTimeLabel.text = [NSString stringWithFormat:@"%lu:0%lu",minute,second];
    } else {
        self.endTimeLabel.text = [NSString stringWithFormat:@"%lu:%lu",minute,second];
    }
}

- (NSInteger)getVideoId
{
    //playingIndexとvideoInfosかvideoIdを取得
    NSDictionary *video = (NSDictionary *)[self.videoInfos objectAtIndex:self.playingIndex];
    NSInteger videoId = (NSInteger)[video valueForKey:@"id"];
    return videoId;
}

- (void)rateVideo:(BOOL)like{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"3837acde800243f85230dd5748388db1" forHTTPHeaderField:@"api-key"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [ud stringForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"Authorization"];
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@videos/%@/rate",apiBaseUrl,[self getVideoId]];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:[ud objectForKey:@"UserId"] forKey:@"user_id"];
    [dict setValue:@"twitter" forKey:@"provider"];
    
    if (like) {
        [dict setValue:@"like" forKey:@"score"];
    } else {
        [dict setValue:@"dislike" forKey:@"score"];
    }
    [dict setValue:@"viewing_time" forKey:@"100"];
    
    NSLog(@"%@",dict);
    
    [manager POST:requestUrl parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        NSLog(@"error:%@", error);
    }];
}

- (void)timerAfter
{
    [self.nextLoadingViewController stopLoadingImage];
    [self.playerView.mediaPlayer playAtIndex:self.playingIndex];
}

- (void)autoHideBar
{
    [self hideBar];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
